# Python Game of Life

A humble implementation of Conway's Game of Life using PyGame and good old elbow grease and shoulder pain. Its meant stand alone besides the pygame and pygame-ui requirement.

![](demo.gif)
# Running 
For now the game is very basic, just launch it and enjoy the show:
```
./main.py
```
There are a couple command line options for now, nothing fancy; I just
wanted two basic controls over how much live cells:

```
>$ ./main.py --help
pygame 1.9.6
Hello from the pygame community. https://www.pygame.org/contribute.html
usage: main.py [-h] [-A ALIVE_INIT] [-t THRESHOLD] [-T TICK]

optional arguments:
  -h, --help            show this help message and exit
  -A ALIVE_INIT, --alive_init ALIVE_INIT
                        score out of 1000 of the total amount of cells that should spawn alive
  -t THRESHOLD, --threshold THRESHOLD
                        score out of 1000 that a cell should spawn alive
  -T TICK, --tick TICK  speed setting of the clock tick

```

# Notes and Disclaimer

I'd just like to declare that I am aware there are a lot of bugs to work out still in this project.
I don't beleive the simulation is total acurate but it is in an interesting enough place to share.
I'll keep working on this until I'm happy with the way the thing behaves, until then enjoy!

Known Problems:
* seems to be somekind of upward bias - this might be due to the way I'm traversing the cellgrid during updates, I will convert to a more randomizable data structure to see if this helps soon.

