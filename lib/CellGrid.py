#!/usr/bin/python3
import random
import time
from .Cell import Cell
"""
			I'm imagining the axis of the cells in this way
				 |	y+
				 |
				 | top
				 | ^
				 | |
				 | *-> right
				  -------- x+
				/ /
			  / v
			 /  front
			/z+
"""
class CellGrid:
	def __init__(self,origin=(0,0,0),vector=(100,100,100),screen=None,rand=False,threshold=800,alive_init=500):
		self.randomize = rand
		self.threshold = threshold
		self.max = vector
		self.alive_init = int((alive_init/1000.0)*(self.max[0]*self.max[1]))
		#print("[*] total :=> %d, alive_init :=> %d" % (self.max[0]*self.max[1],self.alive_init))
		self.origin = origin
		self.screen = screen
		self.cells = [[[ Cell(screen=screen,is_alive=False) for k in range(self.max[2])] for j in range(self.max[1])] for i in range(self.max[0])]
		self._init_grid()

	def render(self):
		self.alives = 0
		for i in range(self.max[0]):
			for j in range(self.max[1]):
				_cell = self.cells[i][j][0]
				_alive = self.is_alive(pos=(i,j,0))
				if _cell.is_alive == True and _alive:
					_cell.pump()	
				elif not(_alive) or (_cell.is_alive == True) and (_cell.nbs > 3 or _cell.nbs < 2):
					_cell.die()
				elif (_cell.is_alive == False and _cell.nbs == 3) or _alive:
					_cell.live()	

				self.alives += self.cells[i][j][0].nbs
				#print("[>] alives: <%d> " % (self.alives),end='\r')	

		for i in range(self.max[0]):
			for j in range(self.max[1]):
				_cell = self.cells[i][j][0]
				if _cell.is_alive:
					_cell.render()
		
	def is_alive(self,pos=(0,0,0)):
		nbs = self.cell_nbs(pos)
		return nbs == 3 or nbs == 2
	
	def cell_nbs(self,pos=(0,0,0)):
		nbs = 0

		top = self.get_top(pos)
		bottom = self.get_bottom(pos)
		right = self.get_right(pos)
		left = self.get_left(pos)

		topleft = self.get_topleft(pos)
		topright = self.get_topright(pos)
		bottomleft = self.get_bottomleft(pos)
		bottomright = self.get_bottomright(pos)
	


		if right != None and right.is_alive == True:
			nbs += 1
		                                             
		if left != None and left.is_alive == True:
			nbs += 1

		if top != None and top.is_alive == True:
			nbs += 1
			if topleft != None and topleft.is_alive == True:
				nbs += 1

			if topright != None and topright.is_alive == True:
				nbs += 1


		if bottom != None and bottom.is_alive == True:
			nbs += 1
			if bottomleft != None and bottomleft.is_alive == True:
				nbs += 1
			if bottomright != None and bottomright.is_alive == True:
				nbs += 1
		self.cells[pos[0]][pos[1]][pos[2]].nbs = nbs
		return nbs

	def has_top(self,pos=(0,0,0)):
		return not(self.get_top(pos) == None)

	def get_top(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]][pos[1]+1][pos[2]]
		except:
			return None

	def has_topleft(self,pos=(0,0,0)):
		return not(self.get_topleft(pos) == None)

	def get_topleft(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]-1][pos[1]+1][pos[2]]
		except:
			return None

	def has_topright(self,pos=(0,0,0)):
		return not(self.get_topright(pos) == None)

	def get_topright(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]+1][pos[1]+1][pos[2]]
		except:
			return None

	def has_bottom(self,pos=(0,0,0)):
		return not(self.get_bottom(pos) == None)

	def get_bottom(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]][pos[1]-1][pos[2]]
		except:
			return None
	def has_bottomleft(self,pos=(0,0,0)):
		return not(self.get_bottomleft(pos) == None)

	def get_bottomleft(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]-1][pos[1]-1][pos[2]]
		except:
			return None

	def has_bottomright(self,pos=(0,0,0)):
		return not(self.get_bottomright(pos) == None)

	def get_bottomright(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]+1][pos[1]-1][pos[2]]
		except:
			return None
	def has_left(self,pos=(0,0,0)):
		return not(self.get_left(pos) == None)

	def get_left(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]-1][pos[1]][pos[2]]
		except:
			return None
	def has_right(self,pos=(0,0,0)):
		return not(self.get_right(pos) == None)

	def get_right(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]+1][pos[1]][pos[2]]
		except:
			return None
	def has_front(self,pos=(0,0,0)):
		return self.get_front(pos[0],pos[1],pos[2]) == None
	def has_back(self,pos=(0,0,0)):
		return self.get_back(pos) == None
	def get_back(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]][pos[1]][pos[2]-1]
		except:
			return False
	def get_front(self,pos=(0,0,0)):
		try:
			return self.cells[pos[0]][pos[1]][pos[2]-1]
		except:
			return False
	def _init_grid(self):
		for i in range(self.max[0]):
			for j in range(self.max[1]):
				#for k in range(self.max[2]):
					#if self.cells[i][j][k] == None:
				cell_position=(self.origin[0]+(i*10),
									self.origin[1]+(j*10),0)
									#self.origin[2]+(k*10))

				if self.randomize == True:
					flip = ((random.random() * 9999 ) % 1000) >= self.threshold
					if flip == True:
						if self.alive_init > 0:
							self.alive_init -= 1
							self.cells[i][j][0] = Cell(screen=self.screen,pos=cell_position,is_alive=True)	
						else:
							self.cells[i][j][0] = Cell(screen=self.screen,pos=cell_position,is_alive=False)	
					else:
						self.cells[i][j][0] = Cell(screen=self.screen,pos=cell_position,is_alive=False)	
							

				else:
					self.cells[i][j][0] = Cell(screen=self.screen,pos=cell_position,is_alive=True)	
				
