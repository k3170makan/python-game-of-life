#!/usr/bin/python3
#from .Cell import Cell
from .CellGrid import CellGrid
import pygame
class Game:
	def __init__(self,origin=(10,10,10),vector=(130,90,1),randomize=False,threshold=815,alive_init=500,tick=20):
		pygame.init()
		self.tick = tick
		self.screen = pygame.display.set_mode([1800, 1000])
		self.running = False
		self.cellgrid = CellGrid(screen=self.screen,
										origin=origin,
										vector=vector,
										rand=randomize,
										threshold=threshold,
										alive_init=alive_init)	

	def run(self):
		self.clock = pygame.time.Clock()
		
		running = True
		while running:
			try:
				self.clock.tick(self.tick)
				for event in pygame.event.get():
					if event.type == pygame.QUIT:
						running = False

				self.screen.fill((0, 0, 0))
				self.cellgrid.render()
				pygame.display.flip()
			except KeyboardInterrupt:
				break

		pygame.quit()
		return 0
