#!/usr/bin/python3
import pygame
import random
import time

"""
			I'm imagining the axis of the cells in this way
				 |	y+
				 |
				 | top
				 | ^
				 | |
				 | *-> right
				  -------- x+
				/ /
			  / v
			 /  front
			/z+
"""

class Cell:
	def __init__(self,pos=(0,0,0),screen=None,is_alive=False):
		self.is_alive = is_alive
		self.age = 0
		self.dead_count = 0
		self.nbs = 0
		self.pos = pos
		self.screen = screen
		self.radius = 5
		self.top = None
		self.bottom = None
		self.left = None
		self.right = None
		self.color = (0, 0, 0)

	def die(self):
		self.dead_count += 1
		self.color = (0, 0, 0)
		self.is_alive = False
		self.age = 0
		#if self.dead_count >= 2000:
		#	self.live()
	def live(self):
		self.dead_count = 0
		self.is_alive = True
		self.color = (255, 255, 255)

	def color_pump(self):
		self.color = (((self.color[0] - 20)%255),((self.color[1] - 30) % 255),(self.color[2] - 10) % 255)
		return
	def pump(self):
		self.age += 1
		self.color_pump()
		#if self.age > 15 and ((random.random()*999) % 100 > 55):
		#	self.die()
	def render(self):
		pygame.draw.circle(self.screen, 
			self.color, 
			(self.pos[0],self.pos[1]), 
			self.radius)             

	def set_top(self,cell=None):
		self.top = cell

	def set_bottom(self,cell=None):
		self.bottom = cell
	
	def set_left(self,cell=None):
		self.left = cell
	
	def set_right(self,cell=None):
		self.right = cell
	
	def set_front(self,cell=None):
		self.front = cell

	def set_back(self,cell=None):
		self.back = cell

