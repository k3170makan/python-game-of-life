#!/usr/bin/python3
import argparse
from lib import Game

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-A","--alive_init",help="score out of 1000 of the total amount of cells that should spawn alive",default=350,type=int)
	parser.add_argument("-t","--threshold",help="score out of 1000 that a cell should spawn alive",default=870,type=int)
	parser.add_argument("-T","--tick",help="speed setting of the clock tick",default=20,type=int)
	args = parser.parse_args()

	g = Game(randomize=True,threshold=args.threshold,alive_init=args.alive_init,tick=args.tick)
	g.run()
